package tb.sockets.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.DefaultCaret;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;


public class MainFrameServer extends JFrame implements ActionListener, Runnable {
		
	
	private final JTextField Txf = new JTextField(30);
    private final JTextPane Txp = new JTextPane();
    private final JButton send = new JButton("Wyślij wiadomość");
    private Scanner in;
    private Thread wyjatek;
    DataOutputStream output;
    private volatile PrintWriter out;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					 MainFrameServer ramka = new MainFrameServer();
					ramka.setVisible(true);
					ramka.startServer();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public MainFrameServer() 
	{
		 setTitle("SERWER");
	     setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	     getRootPane().setDefaultButton(send);
	     add(Txf, BorderLayout.NORTH);
	     add(new JScrollPane(Txp), BorderLayout.CENTER);
	     add(send, BorderLayout.SOUTH);
	     send.setBackground(new Color(255, 255, 255));
	     setLocation(100, 200);
	     setPreferredSize(new Dimension(240,300));
	     pack();
	     send.addActionListener(this);
	      	       
	     Txp.setCaretColor(Color.BLUE);
	     Txp.setBackground(new Color (255,255,255));
	        DefaultCaret caret = (DefaultCaret) Txp.getCaret();
	        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	        display("SERWER", Color.BLACK);
	        wyjatek = new Thread(this);
	}
	
	public void startServer() 
	{
        wyjatek.start();
    }
	
	public void run()
	{
		ServerSocket sSock = null;
		Socket sock = null;
		try {
			 sSock = new ServerSocket(6668);
			 sock = sSock.accept();
				    
			in = new Scanner(sock.getInputStream());
            out = new PrintWriter(sock.getOutputStream(), true);
			
			display("Połączono", Color.BLACK);
			
			String linia = new String();
			while (linia.indexOf("narazie") == -1) {
								
	             linia = in.nextLine();	             
	   	         display("Klient: " + linia, Color.BLUE);
	           }
			
			display("Klient się rozłączył", Color.BLACK);  
			linia = null;

			
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {	
			try {
				output.close();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			in.close();
			out.close();
			try {
				sock.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				sSock.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Txf.setEnabled(false);
		}
	}

	 private void display(final String s, Color c) {
	        EventQueue.invokeLater(new Runnable() {
	            //@Override
	            public void run() {
	                appendToFrame(Txp, s + "\n", c);
	            }
	        });
	    }
	 
	 private void appendToFrame(JTextPane Tp, String wiad, Color c)
	    {
	        StyleContext style = StyleContext.getDefaultStyleContext();
	        AttributeSet as = style.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

	        as = style.addAttribute(as, StyleConstants.FontFamily, "Times New Roman");
	        as = style.addAttribute(as, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

	        int length = Tp.getDocument().getLength();
	        Tp.setCaretPosition(length);
	        Tp.setCharacterAttributes(as, false);
	        Tp.replaceSelection(wiad);
	    }
	 
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		String s = Txf.getText();
        if (out != null) {
				out.println(s);
        }
        display("SERWER: " + s, Color.RED);
        Txf.setText("");
	}	
}