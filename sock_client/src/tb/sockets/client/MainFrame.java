package tb.sockets.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.DefaultCaret;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.JButton;

public class MainFrame extends JFrame implements ActionListener, Runnable{

	private final JTextField Txf = new JTextField(30);
    private final JTextPane Txp = new JTextPane();
    private final JButton send = new JButton("Wyślij wiadomość");
    private Scanner in;
    private Thread wyjatek;
    DataOutputStream output;
    private volatile PrintWriter out;
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame ramka = new MainFrame();
					ramka.setVisible(true);
					ramka.startClient();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() 
	{
		setTitle("KLIENT");
	     setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	     	getRootPane().setDefaultButton(send);
	     add(Txf, BorderLayout.NORTH);
	     add(new JScrollPane(Txp), BorderLayout.CENTER);
	     add(send, BorderLayout.SOUTH);
	     	send.setBackground(new Color(255, 255, 255));
	     	setLocation(100, 200);
	     	setPreferredSize(new Dimension(240,300));
	     pack();
	     send.addActionListener(this);

	     Txp.setCaretColor(Color.BLUE);
	     Txp.setBackground(new Color (255,255,255));
	        DefaultCaret caret = (DefaultCaret) Txp.getCaret();
	        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	        display("KLIENT", Color.BLACK);
			wyjatek = new Thread(this);

	}
	
	public void startClient()
	{
		wyjatek.start();
	}
	
	public void run()
	{
	String NazwKomp = null;
	Socket sock = null;
			try{
				NazwKomp = InetAddress.getLocalHost().getHostName();
				  System.out.println(NazwKomp);
				  }
			catch (Exception e){
				  System.out.println("Wyłapano wyjątek ="+e.getMessage());
				  }
			
			try {
				sock = new Socket(NazwKomp, 6668);			
							
				in = new Scanner(sock.getInputStream());
	            out = new PrintWriter(sock.getOutputStream(), true);
	            
				display("Połączono", Color.BLACK);
				
				String linia = new String();
				while (linia.indexOf("narazie") == -1) {
		             linia = in.nextLine();
		             display("SERWER: " + linia, Color.BLUE);
		           }
								
				display("Serwer się rozłączył", Color.BLACK);
				linia = null;
					
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Błąd");
			}
			finally {
				try {
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				in.close();
				out.close();
				try {
					sock.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Txf.setEnabled(false);
			}
		}
	
    //@Override
    public void actionPerformed(ActionEvent e) {
    	
        String s = Txf.getText();
        if (out != null) {
			out.println(s);
        }
        display("Klient: " + s, Color.RED);
        Txf.setText("");
    }
    
    private void display(final String s, Color c) {
        EventQueue.invokeLater(new Runnable() {
            //@Override
            public void run() {
            	  appendToFrame(Txp, s + "\n", c);
            }
        });
    }
    
    private void appendToFrame(JTextPane Tp, String wiad, Color c)
    {
        StyleContext style = StyleContext.getDefaultStyleContext();
        AttributeSet as = style.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        as = style.addAttribute(as, StyleConstants.FontFamily, "Times New Roman");
        as = style.addAttribute(as, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int length = Tp.getDocument().getLength();
        Tp.setCaretPosition(length);
        Tp.setCharacterAttributes(as, false);
        Tp.replaceSelection(wiad);
    }
    
}